import {createBrowserHistory} from 'history';
import React from 'react';
import {Router as BrowserRouter} from 'react-router-dom';
import './App.css';
import Main from './main'

const history = createBrowserHistory()

function App() {
  return (
    <BrowserRouter history={history}>
      <Main />
    </BrowserRouter>
  );
}

export default App;
