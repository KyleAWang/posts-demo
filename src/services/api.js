import axios from 'axios'
import {
  getPostsResponseWrapper, 
  getCommentsResponseWrapper, 
  getPostsResponseErrorWrapper, 
  getCommentsResponseErrorWrapper,
  getPostResponseWrapper,
  getPostResponseErrorWrapper  
} from './helper/responseWrapper'

axios.defaults.baseURL = 'https://jsonplaceholder.typicode.com'

const isLogEnabled = process.env.API_LOG_ENABLED || false

export const getPosts = async () => {
  try {
    const response = await axios.get('/posts')
    return getPostsResponseWrapper(response)
  } catch (error) {
    return getPostsResponseErrorWrapper(error)
  }
}
export const getComments = async (postId) => {
  try {
    const response = await axios.get(`/posts/${postId}/comments`)
    return getCommentsResponseWrapper(response)
  } catch (error) {
    return getCommentsResponseErrorWrapper(error)
  }
}

export const getPost = async (postId) => {
  try {
    const response = await axios.get(`/posts/${postId}`)
    return getPostResponseWrapper(response)
  } catch (error) {
    return getPostResponseErrorWrapper(error)
  }
}

axios.interceptors.request.use((config) => {
  if (isLogEnabled){
    // TODO: Use logger lib instead of console
    console.log('request config', config)
  }
  return config
})

axios.interceptors.response.use((response) => {
  if (isLogEnabled) {
    // TODO: Use logger lib instead of console
    console.log('response', response)
  }
  return response
})