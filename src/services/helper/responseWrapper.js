import {prop} from "ramda"

export const getPostsResponseWrapper = (response) => 
  response?.data?.map(item => ({
    userId: prop('userId', item),
    id: prop('id', item),
    title: prop('title', item),
    body: prop('body', item),
  }))

export const getPostsResponseErrorWrapper = (error) => {
  console.error(error)
  return []
} 

export const getPostResponseWrapper = (response) => 
  ({
    userId: prop('userId', response?.data),
    id: prop('id', response?.data),
    title: prop('title', response?.data),
    body: prop('body', response?.data),
  })
export const getPostResponseErrorWrapper = (error) => {
  console.error(error)
  return []
} 

export const getCommentsResponseWrapper = (response) => 
  response?.data?.map(item => ({
    postId: prop('postId', item),
    id: prop('id', item),
    name: prop('name', item),
    body: prop('body', item),
    email: prop('email', item)
  }))

export const getCommentsResponseErrorWrapper = (error) => {
  console.error(error)
  return []
} 
