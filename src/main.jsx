import React from 'react'
import {createUseStyles} from 'react-jss'
import { Route } from 'react-router-dom'
import Routes from './routes'

const useStyles = createUseStyles({
  wrapper: {
    maxWidth: '1024px',
    margin: 'auto',
    height: '100%',
  },
})

const Main = () => {
  const classes = useStyles()

  return <>
    <div className={classes.wrapper}>
      <Route component={Routes}/>
    </div>
  </>
}

export default Main