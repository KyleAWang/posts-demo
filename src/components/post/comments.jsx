import React from 'react'
import PropTypes from 'prop-types'
import Comment from './comment'


const Comments = ({comments}) => {
  console.log('comments', comments)
  return <div>
    {
      comments?.map(comment => {
        return <Comment key={comment.id} comment={comment} />
      })
    }
  </div>
}
Comments.propTypes = {
  comments: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
    body: PropTypes.string
  })).isRequired
}

export default Comments