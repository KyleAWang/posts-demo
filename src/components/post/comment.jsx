import React from 'react'
import PropTypes from 'prop-types'

const Comment = ({comment: {body, name, email}}) => {
  console.log('name body', body, name, email)
  return (<>
      <hr />
      <div>
        <p>{body}</p>
      </div>
    </>)
}
Comment.propTypes = {
  comment: PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
    body: PropTypes.string
  }).isRequired
}

export default Comment