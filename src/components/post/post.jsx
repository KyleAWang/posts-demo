import React, {useEffect, useState} from 'react'
import PropTypes from 'prop-types'
import {createUseStyles} from 'react-jss'
import {useLocation} from 'react-router-dom'
import {getComments, getPost} from '../../services/api'
import Comments from './comments'

const useStyles = createUseStyles({
  title: {
    textAlign: 'center'
  },
})

const Post = (props) => {
  const classes = useStyles()
  const postId = props.match.params.postId
  const location = useLocation()
  const [post, setPost] = useState({})
  const [comments, setComments] = useState([])
  useEffect(() => {
    (async () => {
      const post = await getPost(postId)
      setPost(post)
      const comments = await getComments(postId);
      setComments(comments)
    })()
  }, [location, postId])

  return <>
    <div className={classes.title}>
      <p>{post.title}</p>
    </div>
    <Comments comments={comments} />
  </>
}

Post.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      postId: PropTypes.string
    })
  })
}

export default Post