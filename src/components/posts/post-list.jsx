import {isEmpty} from 'ramda'
import React, {useEffect, useState} from 'react'
import {createUseStyles} from 'react-jss'
import {useLocation} from 'react-router-dom'
import {getPosts} from '../../services/api'
import Title from './title'

const useStyles = createUseStyles({
  wrapper: {
    width: '100%',
  },
})

const PostList = props => {
  const classes = useStyles()
  const location = useLocation()
  const [posts, setPosts] = useState([])
  console.log('location', location)
  useEffect(() => {
    (async () => {
      const posts = await getPosts();
      setPosts(posts)
    })()
  }, [location])

  return <div className={classes.wrapper}>
    {
      !isEmpty(posts) && posts.map(props => {
        return (
          <div key={props.id} >
          <Title {...props}/>
          </div>
        )
      })
    }
  </div>
}

export default PostList