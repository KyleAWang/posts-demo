import React from 'react'
import PropTypes from 'prop-types'
import {useHistory} from 'react-router-dom'

const Title = ({userId, id, title, body}) => {
  const history = useHistory()
  const onClick = (e, url) => {
    e.preventDefault()
    history.push(url)
  }
  
  return (
    <div>
      <a 
        href="#" 
        onClick={event => onClick(event, `/posts/${id}`)}
      >{title}</a> 
    </div>
    )
}

Title.propTypes = {
  userId: PropTypes.number,
  id: PropTypes.number.isRequired,
  title: PropTypes.string,
  body: PropTypes.string.isRequired
}


export default Title