import React from 'react'
import { createUseStyles } from 'react-jss'
import PostList from './post-list'

const useStyles = createUseStyles({
  wrapper: {
    maxWidth: '1024px',
    margin: 'auto',
    height: '100%'
  },
})


const Posts = () => {
  const classes = useStyles()
  return <div className={classes.wrapper}>
    <PostList />
  </div>
}

export default Posts