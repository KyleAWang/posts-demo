import React from 'react'
import {Route, Switch} from 'react-router-dom'
import Post from './components/post'
import Posts from './components/posts'

const Routes = props =>{
  return <>
    <Switch>
      <Route exact path="/" component={Posts} />
      <Route path="/posts/:postId" component={Post} />
    </Switch>
  </>
}

export default Routes